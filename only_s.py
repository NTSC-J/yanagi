#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
import sys
import math
import time
from pygame.locals import *
import hightail
import hightail.directionsensor as ds
import config

sensor = ds.DirectionSensor()

pygame.init()

if config.FULLSCREEN:
    screen = pygame.display.set_mode(config.SCREEN_SIZE, FULLSCREEN, 32)
else:
    screen = pygame.display.set_mode(config.SCREEN_SIZE)

class Player(object):
    def __init__(self):
        self.atk = 0

class Enemy(object):
    def __init__(self):
        self.img0 = pygame.image.load("en0.png").convert_alpha()
        self.img1 = pygame.image.load("en1.png").convert_alpha()
        self.img2 = pygame.image.load("en2.png").convert_alpha()
        self.img3 = pygame.image.load("en3.png").convert_alpha()
        self.img4 = pygame.image.load("en4.png").convert_alpha()
        self.damage = 0

thePlayer = Player()
theEnemy = Enemy()

#準備-----------------------------------------------------------
pygame.display.set_caption("key event")
sysfont = pygame.font.SysFont(None, 80)

x = 0
y = 0

img_rect = theEnemy.img0.get_rect()

clock = pygame.time.Clock()
radian = 0
deg = 0
radius = 500
velo = 10
posi = 0
frame = 0
random = 0
add = 0
pygame.key.set_repeat()

#ゲーム---------------------------------------------------
while True:
    clock.tick(20)        #fps設定
    
    thePlayer.atk = 2 #攻撃力設定
    
    screen.fill((100,100,255))

    deg = -sensor.getAngle()
    
    radian = deg * math.pi /180
    
    x = radius * math.cos(radian) + 160
    y = radius * math.sin(radian) + 620

    if -75 <= deg and deg <= 1:
        alert = sysfont.render("Right", True, (0,0,0))
    elif -105 <= deg and deg <= -75:
        alert = sysfont.render("ATTACK!", True, (0,0,0))
    elif -225 <= deg and deg <= -105:
        alert = sysfont.render("Left", True, (0,0,0))
    elif -315 <= deg and deg <= -225:
        alert = sysfont.render("Back", True, (0,0,0))
    elif -360 <= deg and deg <= -315:
        alert = sysfont.render("Right", True, (0,0,0))
    screen.blit(alert,(90,100))
    
    img_rect.center = (x,y)
    
    #キー入力処理------------------------------------------
    for event in pygame.event.get():
        if event.type == QUIT:
            ds.shinu(sensor)
        if event.type == KEYDOWN:
            if event.key in config.QUIT_KEYS:
                ds.shinu(sensor)
            
            if event.key in config.ATTACK_KEYS:
                if deg <= -75 and deg >= -105:
                    theEnemy.damage += ACK
                        
            if event.key in config.RANDOM_KEYS:
                randeg = random
                damage = 0
    
    if theEnemy.damage in range(0,6):
        screen.blit(theEnemy.img0, img_rect)
    elif theEnemy.damage in range(6,11):
        pygame.transform.scale2x(screen,DestSurface = None)
    elif theEnemy.damage in range(11,16):
        screen.blit(theEnemy.img2, img_rect)
    elif theEnemy.damage in range(16,21):
        screen.blit(theEnemy.img3, img_rect)
    elif theEnemy.damage in range(21,30):
        screen.blit(theEnemy.img4, img_rect)    
    elif theEnemy.damage >= 30:
        theEnemy.damage = 0
        pass
        
    #var = sysfont.render(str(radian),True,(100,100,100))    #現在の角度を表示
    #screen.blit(var,(150,150))                             #（弧度法）
    
    degvar = sysfont.render(str(deg),True,(0,0,0),(255,255,255))
    screen.blit(degvar,(150,150))        #現在の角度を表示（360°）
    
    pygame.display.update()

