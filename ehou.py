#!/usr/bin/env python
# coding: utf-8

import pygame
from pygame.locals import *
from pygame import gfxdraw
import sys
import math
import datetime
import hightail
import hightail.directionsensor as ds
import hightail.direction
import hightail.colors
import hightail.polygon
import config

BGColor = hightail.colors.Black
FGColor = hightail.colors.Red
Center = (int(config.SCREEN_SIZE[0] / 2), int(config.SCREEN_SIZE[1] / 2))
VisibleAngle = math.pi * 0.5

pygame.init()

if(config.FULLSCREEN):
    screen = pygame.display.set_mode(config.SCREEN_SIZE, FULLSCREEN, 32)
else:
    screen = pygame.display.set_mode(config.SCREEN_SIZE)

def get_ehou(y = datetime.datetime.today().year % 10):
    if y in {4, 9}: # 甲・己
        return hightail.direction.Direction().setInRadians(math.pi / 12)
    elif y in {0, 5}: # 乙・庚
        return hightail.direction.Direction().setInRadians(13 * math.pi / 12)
    elif y in {1, 3, 6, 8}: # 丙・辛・戊・癸
        return hightail.direction.Direction().setInRadians(19 * math.pi / 12)
    elif y in {2, 7}: # 丁・壬
        return hightail.direction.Direction().setInRadians(7 * math.pi / 12)

class God(object):
    def __init__(self):
        img = pygame.image.load('img/God_of_Toshitoku.png').convert_alpha()
        newsize = (int(img.get_width() * (config.SCREEN_SIZE[1]) / img.get_height()), config.SCREEN_SIZE[1])
        self.surface = pygame.transform.smoothscale(img, newsize)
        self.rect = self.surface.get_rect()
        self.rect.centery = Center[1]
        self.rect.centerx = Center[0]

    def update(self, diff):
        self.rect.centerx = int(Center[0] + diff * config.SCREEN_SIZE[0] / VisibleAngle)

pygame.display.set_caption(u'EHOU')
sysfont = pygame.font.SysFont(None, 28)
ehou = get_ehou().getInRadians()
clock = pygame.time.Clock()
sensor = ds.DirectionSensor()
octagon1 = hightail.polygon.RPolygon(Center, 180, 8, math.pi / 8)
octagon1.draw((500, 500), hightail.colors.Red)
god = God()

# game loop
while True:
    clock.tick(60)
    screen.fill(BGColor)
    R = 180
    direction = sensor.getAngleInRad()
    diff = hightail.direction.difference(ehou, direction)
    pygame.gfxdraw.aacircle(screen, Center[0], Center[1], R, FGColor)
    screen.blit(octagon1.surface, (0, 0))
    
    pygame.draw.aaline(screen, FGColor, Center, (Center[0] + R * math.cos(ehou), Center[1] - R * math.sin(ehou)))
    pygame.draw.aaline(screen, FGColor, Center, (Center[0] + R * math.cos(direction), Center[1] - R * math.sin(direction)))
    
    #text = sysfont.render(u"Ehou = %f, you = %f, diff = %f" % (ehou, direction, diff), True, hightail.colors.Red)
    #screen.blit(text, (0, 0))

    god.update(diff)
    screen.blit(god.surface, god.rect)

    pygame.display.update()
    # event process
    for event in pygame.event.get():
        if event.type == QUIT:
            ds.shinu(sensor)
            sys.exit()

