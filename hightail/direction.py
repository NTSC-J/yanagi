#!/usr/bin/env python
# coding: utf-8

import math

class NoSuchDirectionError(Exception):
    pass

# stores angle in radians, and converts it to several formats
class Direction(object):
    def __init__(self):
        self.angle = float()

    def sanitize(self, floor = 0.0):
        while self.angle < floor:
            self.angle += 2.0 * math.pi
        while self.angle > floor + 2.0 * math.pi:
            self.angle -= 2.0 * math.pi
        return self

    def __add__(self, other):
        return Direction().setInRadians(self.getInRadians() + other.getInRadians())

    def __iadd__(self, other):
        self = self + other
        return self

    def __sub__(self, other):
        return Direction().setInRadians(self.getInRadians() - other.getInRadians())

    def __isub__(self, other):
        self = self - other
        return self

    def __neg__(self):
        self.angle = -self.angle
        self.sanitize()
        return self

    def __complex__(self):
        return complex(math.cos(self.angle), math.sin(self.angle))

    def setInRadians(self, a):
        self.angle = a
        self.sanitize()
        return self
    
    def getInRadians(self):
        return self.angle

    def setInDegrees(self, a):
        self.angle = math.radians(a)
        self.sanitize()
        return self

    def getInDegrees(self):
        return math.degrees(self.angle)

    def setInJapanese(self, string):
        s = string.lower()
        if s in {u'東'}:
            self.angle = 0.0
        elif s in {u'北'}:
            self.angle = math.pi * 0.5
        elif s in {u'西'}:
            self.angle = math.pi
        elif s in {u'南'}:
            self.angle = math.pi * 1.5
        else:
            raise NoSuchDirectionError()

def difference(a, b):
    return (Direction().setInRadians(b) - Direction().setInRadians(a)).sanitize(-math.pi).angle

if __name__ == '__main__':
    print('Hello!')

