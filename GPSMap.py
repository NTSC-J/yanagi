#!/usr/bin/env python
# coding: utf-8

import urllib.request
import urllib.parse
from pygame.locals import *
from xml.etree import ElementTree
import pygame
import threading
import socket
import time
import sys
import re
import io
import hightail
import hightail.directionsensor as ds
sensor = ds.DirectionSensor()
yobi = 0 #YahooAPIの呼び出し数カウント
class YOLPMapImage:
	def __init__(self, image, width, height, co, coUL, coUR, coDL, coDR):
		self.ppcX = 0.0
		self.ppcY = 0.0
		self.image = image
		self.width = width
		self.height = height
		self.co = co
		self.coUL = coUL
		self.coUR = coUR
		self.coDL = coDL
		self.coDR = coDR
		self.ppcX = width / (coDR[0] - coUL[0])
		self.ppcY = height / (coUL[1] - coDR[1])

class YOLPMap:
	APPID = 'dj0zaiZpPWxMQkJvdjB1QmlzNyZzPWNvbnN1bWVyc2VjcmV0Jng9NzI-'
	MAPURL = 'http://map.olp.yahooapis.jp/OpenLocalPlatform/V1/static'
	HOST = ''
	PORT = 4643
	IMAGE_REQEST_WIDTH = 2000
	IMAGE_REQEST_HEIGHT = 2000
	IMAGE_VIEW_WHIDTH = 800
	IMAGE_VIEW_HEIGHT = 800
	def __init__(self):
		self.ido = 35.6387617
		self.keido = 139.3030693
		#zoomLevelは1(広域)～20(詳細)
		self.mapImage = self.getImage(self.ido, self.keido)
		self.processedMapImage = None
		self.threadAlile = True
		self.posChanged = True
		self.needImageRefresh = False
		self.mapConfig = {	"zoomLevel" 	: 18 ,
							"imageWidth" 	: self.IMAGE_REQEST_WIDTH ,
							"imageHeight"	: self.IMAGE_REQEST_HEIGHT ,
							"iamgeFormat"	: 'jpeg' ,
							"imageQuality"	: 50 
						 }
		self.GPSTh = threading.Thread(target = self.startGPSDataStreamingThread)
		self.getImageTh = threading.Thread(target = self.startgetImageThread)
		self.GPSTh.start()
		self.getImageTh.start()

	def setZoomLevel(self,level):
		self.zoomLevel = level

	def getImage(self, ido, keido, zoom = 18, width = 2000, height = 2000, imgformat = 'jpg', quality = 50):
		#Yahooの地図画像をとってきてYOLPMapImageで返す
		req = {}
		req['appid'] = self.APPID
		req['lat'] = str(ido)
		req['lon'] = str(keido)
		req['z'] = str(zoom)
		req['width'] = str(width)
		req['height'] = str(height)
		req['quality'] = str(quality)
		req['output'] = imgformat

		url_req = urllib.parse.urlencode(req)
		full_url = self.MAPURL + '?' + url_req
		data = urllib.request.urlopen(full_url)
		image_str = data.read()
		image_file = io.BytesIO(image_str)
		img = pygame.image.load(image_file).convert_alpha()

		req['output'] = 'xml'
		url_req = urllib.parse.urlencode(req)
		full_url = self.MAPURL + '?' + url_req
		data = urllib.request.urlopen(full_url)
		rawData = data.read()
		rawDataFile = io.BytesIO(rawData)
		tree = ElementTree.parse(rawDataFile)
		root = tree.getroot()
		coUL = list(map(float, root.find('.//{urn:yahoo:jp:olp:static}Coordinate-UL').find('{urn:yahoo:jp:olp:static}Coordinates').text.split(',')))
		coUR = list(map(float, root.find('.//{urn:yahoo:jp:olp:static}Coordinate-UR').find('{urn:yahoo:jp:olp:static}Coordinates').text.split(',')))
		coDL = list(map(float, root.find('.//{urn:yahoo:jp:olp:static}Coordinate-DL').find('{urn:yahoo:jp:olp:static}Coordinates').text.split(',')))
		coDR = list(map(float, root.find('.//{urn:yahoo:jp:olp:static}Coordinate-DR').find('{urn:yahoo:jp:olp:static}Coordinates').text.split(',')))
		return YOLPMapImage(img, width, height, [ido, keido], coUL, coUR, coDL, coDR)

	def startGPSDataStreamingThread(self):
		#UDPで送られてきたカンマ区切りの座標データをどうにかする
		self.clientsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.clientsock.bind((self.HOST, self.PORT))
		while self.threadAlile:
			recv_msg = self.clientsock.recv(1024)
			recv_pos = recv_msg.decode('utf-8').split(",")
			self.ido = float(recv_pos[0])
			self.keido = float(recv_pos[1])
			self.posChanged = True #新しくデータが送られてきたなら絶対に座標は変わってるはず
	def startgetImageThread(self):
		while self.threadAlile:
			if self.needImageRefresh:
				print("load Image")
				self.mapImage = self.getImage(	self.ido, 
												self.keido,
												self.mapConfig['zoomLevel'],
												self.mapConfig['imageWidth'], 
												self.mapConfig['imageHeight'], 
												self.mapConfig['iamgeFormat'], 
												self.mapConfig['imageQuality']
											 )
				print("imageChange")
				self.needImageRefresh = False
			time.sleep(1.0)
	def kill(self):
		self.threadAlile = False
		self.clientsock.close()

	def getMapImage(self):
		if self.posChanged:
			print('pos changed')
			retImage = pygame.Surface((self.IMAGE_VIEW_WHIDTH,self.IMAGE_VIEW_HEIGHT))
			offsetX = (retImage.get_rect().width  - self.mapImage.image.get_rect().width)  / 2.0 - ((self.keido   - self.mapImage.co[1]) * self.mapImage.ppcX)
			offsetY = (retImage.get_rect().height - self.mapImage.image.get_rect().height) / 2.0 + ((self.ido - self.mapImage.co[0]) * self.mapImage.ppcY)
			if offsetX > 0 or offsetY > 0 or offsetX < (retImage.get_rect().width  - self.mapImage.image.get_rect().width) or offsetY < (retImage.get_rect().height - self.mapImage.image.get_rect().height):
				self.needImageRefresh = True
				print('need more image')
			retImage.blit(self.mapImage.image, (offsetX,offsetY))
			self.processedMapImage = retImage
			self.posChanged = False
			return retImage
		else:
			return self.processedMapImage

def rotate(screen , picture , rect , angle):
	rotated = pygame.transform.rotate(picture , angle)
	size = rotated.get_size()
	pos = (rect.x + rect.w/2 - size[0]/2 , rect.y + rect.h/2 - size[1]/2)
	screen.blit(rotated , pos)

SCREEN_SIZE = (640, 480)
pygame.init()
screen = pygame.display.set_mode(SCREEN_SIZE)
pygame.display.set_caption(u"透明色の指定")
Ymap = YOLPMap()
dx = 0.00
angle = 0.0
clock = pygame.time.Clock()
while True:
	clock.tick(120)
	img = Ymap.getMapImage()
	pressed_keys = pygame.key.get_pressed()
	if pressed_keys[K_a]:
		angle += 0.5
	elif pressed_keys[K_d]:
		angle -= 0.5
	#angle = sensor.getAngle()
	screen.fill((100,100,255))
	rotate(screen,img,pygame.Rect(
								 (SCREEN_SIZE[0] - img.get_rect().width) / 2,
								 (SCREEN_SIZE[1] - img.get_rect().height) / 2,
								 img.get_rect().width,
								 img.get_rect().height),
								 angle
								 )
	pygame.draw.rect(screen, (255, 0, 0), (screen.get_rect().width / 2 -5, screen.get_rect().height / 2 -5, 5, 5) )
	pygame.display.update()
	for event in pygame.event.get():
		if event.type == QUIT:
			sensor.shine()
			Ymap.kill()
			sys.exit()
		if event.type == KEYDOWN:  # キーを押したとき
			# ESCキーならスクリプトを終了
			if event.key == K_ESCAPE:
				sensor.shine()
				Ymap.kill()
				sys.exit()
