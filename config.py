#!/usr/bin/env python
#! coding: utf-8

# 全てのゲームで共通する設定

import pygame
from pygame.locals import *

SCREEN_SIZE = (640, 480)
FULLSCREEN = False

QUIT_KEYS = [K_ESCAPE, K_q]
ATTACK_KEYS = [K_SPACE, K_z]
RANDOM_KEYS = [K_r]

