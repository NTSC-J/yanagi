#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
import sys
import math
import threading
import time
import serial
from pygame.locals import *

#センサーの処理-----------------------------------
sensorIO = 1  #センサーONなら１


class DirectionSensor(object):
	__instance = None
	angle = 0.0
	ikiru = True
	@staticmethod
	def __new__(cls, *args):
		if cls.__instance is None:
			cls.__instance = super(DirectionSensor, cls).__new__(cls, *args)
			cls.__instance.startSensor()
		return cls.__instance
	def getSensorValue(self):
		#sp = serial.Serial('/dev/ttyACM0')
		sp = serial.Serial('COM5')
		while DirectionSensor.ikiru:
			try:
				line = sp.readline()
				linestr = str(line)[2:].replace('\\r\\n\'','')
				#pprint(float(linestr))
				DirectionSensor.angle = float(linestr)
			except ValueError:
				pass
	def startSensor(self):
		self.th = threading.Thread(target=self.getSensorValue)
		self.th.start()
	def getAngle(self):
		return DirectionSensor.angle
	def shine(self):
		DirectionSensor.ikiru = False
	def __init__(self):
		super(DirectionSensor, self).__init__()
#--------------------------------------------------------------
def shinu(ss):
	ss.shine()
	sys.exit()

SCREEN_SIZE = (320, 240)
if sensorIO == 1:
	sensor = DirectionSensor()
else:
	pass
pygame.init()

#コメントアウトで全画面切り替え----------------------------
screen = pygame.display.set_mode(SCREEN_SIZE)
#screen = pygame.display.set_mode(SCREEN_SIZE, FULLSCREEN, 32)


#準備-----------------------------------------------------------
pygame.display.set_caption("key event")
sysfont = pygame.font.SysFont(None, 80)

x = 0
y = 0

img0 = pygame.image.load("en0.png").convert_alpha()
img1 = pygame.image.load("en1.png").convert_alpha()
img2 = pygame.image.load("en2.png").convert_alpha()
img3 = pygame.image.load("en3.png").convert_alpha()
img4 = pygame.image.load("en4.png").convert_alpha()


img_rect = img0.get_rect()

clock = pygame.time.Clock()
damege = 0
radian = 0
deg = -270
radius = 500
velo = 10
posi = 0
frame = 0
random = 0
add = 0
ACK = 0
pygame.key.set_repeat()

#ゲーム---------------------------------------------------
while True:
	frame +=1
	
	if frame == 360:
		frame = 0

	if sensorIO == 0:
		if frame % 10 == 0:
			random = frame
	
	clock.tick(20)		#fps設定
	
	ACK = 2 #攻撃力設定
	
	screen.fill((100,100,255))
	if sensorIO == 1:
		deg = -sensor.getAngle()
	
	else:
		pass
	
	radian = deg * math.pi /180
	
	x = radius * math.cos(radian) + 160
	y = radius * math.sin(radian) + 620
	
	if sensorIO == 0:
		alert = sysfont.render("", True, (0,0,0))
	
	if deg in range(-75,1):
		alert = sysfont.render("Right", True, (0,0,0))
	elif deg in range(-105 ,-75):
		alert = sysfont.render("ATTACK!", True, (0,0,0))
	elif deg in range(-225,-105):
		alert = sysfont.render("Left", True, (0,0,0))
	elif deg in range(-315,-225):
		alert = sysfont.render("Back", True, (0,0,0))
	elif deg in range(-360,-315):
		alert = sysfont.render("Right", True, (0,0,0))
	
	
	img_rect.center = (x,y)
	
	#キー入力処理------------------------------------------
	pressed_keys = pygame.key.get_pressed()
	if sensorIO == 0:
		if pressed_keys[K_RIGHT]:
			deg -= velo
			if deg == -360:
				deg = 0
		if pressed_keys[K_LEFT]:
			deg += velo
			if deg == velo:
				deg = -360 + velo
	else:
		pass
				
	for event in pygame.event.get():
		if event.type == QUIT:
			shinu(sensor)
		if event.type == KEYDOWN:
			if event.key == K_ESCAPE:
				shinu(sensor)
			
			if event.key == K_SPACE:
				if deg <= -75 and deg >= -105:
					if sensorIO == 0:
						damege += ACK
					else:
						damege += ACK
						
			if event.key == K_r:
				if sensorIO == 0:
					deg = random
				else:
					randeg = random
					damege = 0
	
	
	if damege in range(0,6):
		screen.blit(img0, img_rect)
	elif damege in range(6,11):
		screen.blit(img1, img_rect)
	elif damege in range(11,16):
		screen.blit(img2, img_rect)
	elif damege in range(16,21):
		screen.blit(img3, img_rect)
	elif damege in range(21,30):
		screen.blit(img4, img_rect)	
	elif damege >= 30:
		damege = 0
		if sensorIO == 0:
			deg = random
		else:
			pass
			randeg = random

	screen.blit(alert,(90,100))
		
	#var = sysfont.render(str(radian),True,(100,100,100))	#現在の角度を表示
	#screen.blit(var,(150,150)) 							#（弧度法）
	
	degvar = sysfont.render(str(deg),True,(0,0,0),(255,255,255))
	screen.blit(degvar,(150,150))		#現在の角度を表示（360°）
	
	pygame.display.update()
	

