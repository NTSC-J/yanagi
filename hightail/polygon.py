#!/usr/bin/env python
# coding: utf-8

import math
import pygame

# 多角形
class Polygon(object):
    def __init__(self):
        self.points = []

    def draw(self, size, color):
        self.surface = pygame.Surface(size, pygame.SRCALPHA, 32).convert_alpha()
        pygame.draw.aalines(self.surface, color, True, self.points)
        return self.surface

# 正多角形
class RPolygon(Polygon):
    def __init__(self, center, radius, vert, initphase = 0.0):
        super().__init__()
        x0 = center[0]
        y0 = center[1]
        for a in [initphase + (i * 2 * math.pi) / vert for i in range(vert)]:
            self.points.append((x0 + radius * math.cos(a), y0 - radius * math.sin(a)))

