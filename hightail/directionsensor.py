#!/usr/bin/env python
# -*- coding: utf-8 -*-
import serial
import threading
import platform
import os
import os.path
import sys
import time
import math

# CONSTANTS
WIN_COM='COM5'
LIN_TTY='/dev/ttyACM0'
COM_SPEED=115200

class DirectionSensor(object):
    thisIsTheFirstInstanceFuckTakishima = True
    angle = float()

    def __init__(self):
        self.alive = True
        self.runningOSType = platform.system()
        if self.runningOSType == 'Linux' and not os.path.exists(LIN_TTY):
            print('WARNING: Cannot detect sensor, stub mode on')
            self.stub = True
        else:
            self.stub = False
            if self.runningOSType == 'Linux':
                try:
                    self.sp = serial.Serial(LIN_TTY, COM_SPEED)
                except Exception as e:
                    print('WARNING: Cannot open serial port, stub mode on')
                    print(e)
                    self.stub = True
            elif self.runningOSType == 'Windows':
                try:
                    self.sp = serial.Serial(WIN_COM, COM_SPEED)
                except Exception as e:
                    print('WARNING: Cannot open serial port, stub mode on')
                    print(e)
                    self.stub = True
            else:
                sys.exit('Your OS is bad')

        if self.thisIsTheFirstInstanceFuckTakishima:
            self.thisIsTheFirstInstanceFuckTakishima = False
            self.startSensor()

    def getSensorValue(self):
        if not self.stub:
            # GARBAGE
            self.sp.readline()
            self.sp.readline()
        while self.alive:
            if self.stub:
                time.sleep(0.01)
                self.angle = float((int(self.angle) + 1) % 360)
            else:
                line = self.sp.readline()
                try:
                    self.angle = float(str(line)[2:].replace('\\r\\n\'',''))
                except ValueError:
                    print('WARNING: Cannot convert SerialPort data')

    def startSensor(self):
        self.th = threading.Thread(target=self.getSensorValue)
        self.th.start()

    def getAngle(self):
        return self.angle

    def getAngleInRad(self):
        return self.angle * math.pi / 180.0

    def shine(self):
        self.alive = False

def shinu(ss):
    ss.shine()
    sys.exit()

